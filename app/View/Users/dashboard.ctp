<div>
<?php 
if (!$admin){
    if($institution_id) {
      echo '<script type="text/javascript">
              localStorage.setItem("ls.institution_id","'.$institution_id.'");
      </script>';
    }
    if($department_id) {
      echo '<script type="text/javascript">
              localStorage.setItem("ls.department_id","'.$department_id.'");
      </script>';
    }
     if($user_id) {
      echo '<script type="text/javascript">
              localStorage.setItem("ls.user_id","'.$user_id.'");
      </script>';
    }
}
?>
    
<?php print "Welcome {$fullname}"; ?>
</div>

<div>
<?php print  "Your last login was at ".$this->Time->nice($modified); ?>
</div>
<br>
<div class="row">
<div class='col-md-3'>
<?php
  echo $this->Html->link(
    $this->Html->image('FormLogo.png', ['alt' => 'CREATE_FORM']),
    [
        'plugin' => 'form_builder',
        'controller' => 'forms',
        'action' => 'blank',
    ],['escape' => false]
); 
?>
  <br><h5> Form Builder</h5>

  </div>
</div>
</p>