<?php
App::uses('AppModel', 'Model');
/**
 * Department Model
 *
 */
class Department extends AppModel {

        public $displayField = 'name';
 
 
        //The Associations below have been created with all possible keys, those that are not needed can be removed
 

        public $belongsTo = ['Institution'];
 

        public $hasMany = ['Degree','Staff','UserRole','SupportTicketSystem.Category','Category','Student'];
 public $validate = [

        'name' => [
            'required' => [
                'rule' => ['notEmpty'],
                'message' => 'You must enter a Department Name.'
            ],
            'unique' => [
                'rule'    => 'isUnique',
                'message' => 'This Department Name has already been taken.'
            ],
        ]
        ];
 

        public function getListByInstitution($cid = null) {
                if (empty($cid)) {
                        return array();
                }
                return $this->find('list', array('joins' => array(
           array(
              'table' => 'degrees',
              'alias' => 'Degree',
              'type'  => 'left',
              'conditions' => array(
              'Degree.department_id = Department.id'
              )
           )
        ),
            'conditions' => array($this->alias . '.institution_id' => $cid, 'NOT' => array( 'Degree.id' => NULL))
                ));
        }
 
}