<?php
App::uses('FormBuilderAppController','FormBuilder.Controller');

class FormResponsesController extends FormBuilderAppController {
     
public function add_response(){
$this->RequestHandler->renderAs($this, 'json');
 if($this->request->is('post')) {
     $this->FormResponse->create();
       if($this->FormResponse->saveMany($this->request->data,array('deep' => true))){
          $message = "save";
       } else {
          $message = "error";
       }
       $this->set(array(
           'data' => $this->request->data,
           'message' => $message,
           '_serialize' => array('data','message')
       ));
 }

}

public function list_response()
{

     $id = $this->request->query('id');
        if (!$id) {
      throw new NotFoundException();
    }
    $this->disableCache();

    $responses = $this->FormResponse->getListByResponse($id);
    $responses = Set::map($responses);
    $this->set(array(
            'responses' => $responses,
            '_serialize' => array('responses')
        ));
}
}
?>