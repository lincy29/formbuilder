<?php
App::uses('FormBuilderAppController','FormBuilder.Controller');

class FormElementsController extends FormBuilderAppController {
     
public $components = array('Paginator','Session');

/* 
helps to fetch all the form element names
*/

public function list_element()
{
     $id = $this->request->query('id');
     if (!$id) {
       throw new NotFoundException();
    }
    $this->disableCache();
    $elements = $this->FormElement->getListByFormElement($id);
    $elements = Set::map($elements);
    $this->set(array(
            'elements' => $elements,
            '_serialize' => array('elements')
        ));
}
}
?>