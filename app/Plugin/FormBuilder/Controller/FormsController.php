<?php
App::uses('FormBuilderAppController','FormBuilder.Controller');
class FormsController extends FormBuilderAppController {
     
public $components = array('Paginator','Session');

/* 
 index functions that display all the records w.r.t the user
*/
public function developer_index() {
   $this->Form->recursive = -1;
   $this->Paginator->settings = array(
  'page' => 1,
  'contain' => ['Institution'=>['fields'=>['name']],'Category'=>['fields'=>['category_name']],'Department'=>['fields'=>['name']]],'fields'=>['id','name','close_date','recstatus']);
    $this->set('forms', $this->Paginator->paginate());
 }

 public function formadmin_index() {
    $this->Form->recursive = -1;
   $this->Paginator->settings = array(
  'page' => 1,
  'contain' => ['Department'=>['fields'=>['name']],'Category'=>['fields'=>['category_name']]],'fields'=>['id','name','close_date','recstatus']);
    $this->set('forms', $this->Paginator->paginate());
 }

 public function formcoordinator_index() {
    $this->Form->recursive = -1;
   $this->Paginator->settings = array(
  'page' => 1,
  'contain' => ['Category'=>['fields'=>['category_name']]],'fields'=>['id','name','close_date','recstatus']);
    $this->set('forms', $this->Paginator->paginate());
 }

/* 
 add functions that adds new records w.r.t the user
*/
   
 public function developer_add() {
	
 }

 public function formadmin_add() {
  
 
 }

 public function formcoordinator_add() {
  
 }

public function form_add(){  
$this->RequestHandler->renderAs($this, 'json');
 if($this->request->is('post')) {
     $this->Form->create();
     $this->Form->set( $this->request->data );
      if ($this->Form->validates()) {
       $message = "save";
       $this->Form->saveAssociated($this->request->data,array('deep' => true));
         
       } else {
          $message = $this->Form->validationErrors;
       }
       $this->set(array(
           'data' => $this->request->data,
           'message' => $message,
           '_serialize' => array('data','message')
       ));
 }

}

/* 
 edit functions that edits old records w.r.t the user
*/

  public function developer_edit($id = null) {
    $this->set(compact('id'));
  }

  public function edit_view() {
  
  $this->RequestHandler->renderAs($this, 'json');
  $id = $this->request->query('id');
  $data = $this->Form->find('first',['contain' => ['FormElement'=>['FormOption']],'conditions' => ['Form.id' => $id]]);
  $data = Set::map($data); 
  return $this->set(['data' => $data , '_serialize' => ['data']]);
}

public function formadmin_edit($id = null) {
   
}

public function formcoordinator_edit($id = null) {
   
}
/*
deactivating particular records i.e making the recstatus zero
 */
public function developer_deactivate($id = null)
{
  
  if (!$this->Form->exists($id)) {
      throw new NotFoundException(__('Invalid Form'));
  }

  if ($this->request->is(array('post','put'))) {
    $this->request->data['Form']['id'] = $id;
    $this->request->data['Form']['recstatus'] = 0;
    if ($this->Form->save($this->request->data, true, array('id','recstatus'))) {
      $this->Session->setFlash(__('It has been deactivated.') , 'alert', array(
        'class' => 'alert-success'
      ));
    } else {
      $this->Session->setFlash(__('It cannot be deactivated. Please, try again.') , 'alert', array(
        'class' => 'alert-success'
      ));
    }
    return $this->redirect(array('controller' => 'forms','action' => 'index'));
  }
}

public function blank(){
  
}

/* 
Gives all the data for a particular form i.e according to form_id the data will b populated
*/
public function list_form()
{

     $id = $this->request->query('id');
        if (!$id) {
      throw new NotFoundException();
    }
    $this->disableCache();

    $forms = $this->Form->getListByForm($id);
    $forms = Set::map($forms);
    $this->set(array(
            'forms' => $forms,
            '_serialize' => array('forms')
        ));
}

/* 
Gives all the data for a particular form i.e according to form_name the data will b populated
*/

public function list_forms()
{

     $id = $this->request->query('id');
        if (!$id) {
      throw new NotFoundException();
    }
    $this->disableCache();

    $forms = $this->Form->getListByFormName($id);
    $forms = Set::map($forms);
    $this->set(array(
            'forms' => $forms,
            '_serialize' => array('forms')
        ));
}
/* 
Helps to view the form selected
*/
public function view(){

}

/* 
Helps to display the drop downs from where we can select the form to be displayed
*/
public function display_response(){
  
}

public function response(){
  
}
public function developer_display(){
  
}

public function formadmin_display(){
  
}
public function formcoordinator_display(){
  
}
}

?>