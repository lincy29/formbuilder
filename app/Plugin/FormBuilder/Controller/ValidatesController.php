<?php
App::uses('FormBuilderAppController','FormBuilder.Controller');
class ValidatesController extends FormBuilderAppController {
     

public function add() {

if($this->request->is('post')) { 
  $this->Validate->create();
      $this->Validate->set( $this->request->data );
      if ($this->Validate->validates()) {
        $errors = "save";
        $this->Validate->save($this->request->data);             
      } else {   
        $errors = $this->Validate->validationErrors;
      }
       $this->set(array(
           'data' => $this->request->data,
           'message' => $errors,
           '_serialize' => array('data','message')
       ));
   }
	
}


}
?>