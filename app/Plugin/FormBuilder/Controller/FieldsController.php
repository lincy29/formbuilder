<?php
App::uses('FormBuilderAppController','FormBuilder.Controller');



class FieldsController extends FormBuilderAppController {

/* 
helps to fetch all the field names
*/

	public function list_fields(){
	    $this->disableCache();
	    $fields = $this->Field->find('list');
	    $fields = Set::map($fields);
	    $this->set(array(
	            'fields' => $fields,
	            '_serialize' => array('fields')
	    ));
	}
}


?>
