<?php
App::uses('FormBuilderAppController','FormBuilder.Controller');

class CategoriesController extends FormBuilderAppController {

public $components = array('Paginator','Session');

/***
index functions for all roles, it displays all records with its details
**/
public function developer_index() {
    $this->Category->recursive = -1;
    $this->Paginator->settings = array(
  'page' => 1,
  'contain' => ['Institution'=>['fields'=>['name']],'Department'=>['fields'=>['name']]],'fields'=>['id','category_name','recstatus']);
    $this->set('categories', $this->Paginator->paginate());
 }

public function formadmin_index() {

    $this->Category->recursive = -1;
    $this->Paginator->settings = array(
  'page' => 1,
  'contain' => ['Department'=>['fields'=>['name']]],'fields'=>['id','category_name','recstatus']);
    $this->set('categories', $this->Paginator->paginate());
 }

public function formcoordinator_index() {
    $this->Category->recursive = 0;
    $this->set('categories', $this->Paginator->paginate());
 }


/***
add functions for all roles, it adds new record
**/
public function developer_add() {

 
}

public function formadmin_add() {
    

}

public function formcoordinator_add() { 

} 

public function add() {
 $this->RequestHandler->renderAs($this, 'json');
 if($this->request->is('post')) {
     $this->Category->create();
      $this->Category->set( $this->request->data );
      if ($this->Category->validates()) {
       $message = "save";
       $this->Category->saveAssociated($this->request->data);
         
       } else {
          $message = $this->Category->validationErrors;
       }
       $this->set(array(
           'data' => $this->request->data,
           'message' => $message,
           '_serialize' => array('data','message')
       ));
 }

}



public function developer_edit($id = null) { 
   if (!$this->Category->exists($id)) {
      throw new NotFoundException(__('Invalid name'));
    }
    if ($this->request->is(array('post', 'put'))) {
      if ($this->Category->save($this->request->data)) {
        $this->Session->setFlash(__('The Category has been saved.'), 'alert', array(
        'class' => 'alert-success'
      ));
        return $this->redirect(array('action' => 'developer_index'));
      } else {
        $this->Session->setFlash(__('The Category could not be saved. Please, try again.'), 'alert', array(
        'class' => 'alert-danger'
      ));
      }
    }
  else {
      $options = array('conditions' => array('Category.' . $this->Category->primaryKey => $id));
      $this->request->data = $this->Category->find('first', $options);
      $institutions = $this->Category->Institution->find('list');
      $departments = $this->Category->Department->find('list');
      $this->set(compact('institutions','departments')); 
    }
}


public function formadmin_edit($id = null) {
    if (!$this->Category->exists($id)) {
      throw new NotFoundException(__('Invalid name'));
    }
    if ($this->request->is(array('post', 'put'))) {
      if ($this->Category->save($this->request->data)) {
        $this->Session->setFlash(__('The Category has been saved.'), 'alert', array(
        'class' => 'alert-success'
      ));
        return $this->redirect(array('action' => 'formadmin_index'));
      } else {
        $this->Session->setFlash(__('The Category could not be saved. Please, try again.'), 'alert', array(
        'class' => 'alert-danger'
      ));
      }
    }
  else {
      $options = array('conditions' => array('Category.' . $this->Category->primaryKey => $id));
      $this->request->data = $this->Category->find('first', $options);
      $departments = $this->Category->Department->find('list');
      $this->set(compact('departments')); 
    }
}

public function formcoordinator_edit($id = null) {
  if (!$this->Category->exists($id)) {
      throw new NotFoundException(__('Invalid name'));
    }
    if ($this->request->is(array('post', 'put'))) {
      if ($this->Category->save($this->request->data)) {
        $this->Session->setFlash(__('The Category has been saved.'), 'alert', array(
        'class' => 'alert-success'
      ));
        return $this->redirect(array('action' => 'formcoordinator_index'));
      } else {
        $this->Session->setFlash(__('The Category could not be saved. Please, try again.'), 'alert', array(
        'class' => 'alert-danger'
      ));
      }
    }
  else {
      $options = array('conditions' => array('Category.' . $this->Category->primaryKey => $id));
      $this->request->data = $this->Category->find('first', $options);
     } 

}

public function deactivate($id = null)
{
 
  if (!$this->Category->exists($id)) {
      throw new NotFoundException(__('Invalid Category'));
  }

  if ($this->request->is(array('post','put'))) {
    $this->request->data['Category']['id'] = $id;
    $this->request->data['Category']['recstatus'] = 0;
    if ($this->Category->save($this->request->data, true, array('id','recstatus'))) {
      $this->Session->setFlash(__('It has been deactivated.') , 'alert', array(
        'class' => 'alert-success'
      ));
    } else {
      $this->Session->setFlash(__('It cannot be deactivated. Please, try again.') , 'alert', array(
        'class' => 'alert-success'
      ));
    }
   return $this->redirect($this->referer());
  }
}


public function list_categories(){
  $this->request->onlyAllow('ajax');
  $id = $this->request->query('id');
  if (!$id) {
    throw new NotFoundException();    
  }
  $this->disableCache();
  $categories = $this->Category->getListByDepartment($id);
  $this->set(compact('categories'));

  $this->set('_serialize',array('categories'));
}

public function list_add_categories() {
     
     $id = $this->request->query('id');
        if (!$id) {
      throw new NotFoundException();
    }
    $this->disableCache();

    $categories = $this->Category->getListByDepartment($id);
    $categories = Set::map($categories);
    $this->set(array(
            'categories' => $categories,
            '_serialize' => array('categories')
        ));
  }

}