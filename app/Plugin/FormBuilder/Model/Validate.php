<?php
App::uses('FormBuilderAppModel','FormBuilder.Model');

class Validate extends FormBuilderAppModel {

 public $validate = array( 
            'name' => array( 
                   'required' => array( 
                   'rule' => 'notEmpty',
                   'required' => true,                   
                   'message' => 'You must enter a Name.'
                   ),

                   'unique' => array( 
                   'rule'    => 'isUnique',
                   'required' => true,
                   'message' => 'This Name has already been taken.'
                   ),       
                )
            );
}

?>