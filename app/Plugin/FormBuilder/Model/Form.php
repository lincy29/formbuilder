<?php
App::uses('FormBuilderAppModel','FormBuilder.Model');

class Form extends FormBuilderAppModel {
 
 public $belongsTo = ['Institution','Department','Category'];
 public $hasMany = ['FormBuilder.FormElement','FormBuilder.FormOption','FormBuilder.FormResponse'];

public $validate = [
        
        'institution_id' => [
            'required' => [
                'rule' => ['notEmpty'],
                'required' => true, 
                'message' => 'Please select institution.'
            ],
        ],
        'department_id' => [
            'required' => [
                'rule' => ['notEmpty'],
                'required' => true, 
                'message' => 'please select department.'
            ],
        ],

        'close_date' => [
            'required' => [
                'rule' => ['notEmpty'],
                'required' => true, 
                'message' => 'please select a date.'
            ],
        ]
    ];



 public function getListByCategories($aid = null) {
    if (empty($aid)) {
      return array();
    }
    return $this->find('list', array(
      'conditions' => array($this->alias . '.category_id' => $aid),
    ));
  }

   public function getListByForm($catid = null) {

        return $this->find('first', array(
            'conditions' => array($this->alias . '.id' => $catid)
        ));
 }
public function getListByFormName($catid = null) {

        return $this->find('list', array(
            'conditions' => array($this->alias . '.category_id' => $catid)
        ));
 }



}

?>