<?php
App::uses('FormBuilderAppModel','FormBuilder.Model');
/**
 * Role Model
 *
 */
class FormElement extends FormBuilderAppModel {


  public $belongsTo = ['FormBuilder.Form'];
  public $hasMany = ['FormBuilder.FormOption'];

  public function getListByFormElement($catid = null) {

        return $this->find('all', array('recursive'=>1,
            'conditions' => array($this->alias . '.form_id' => $catid)
        ));
 }

}
