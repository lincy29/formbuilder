<script type="text/javascript">
 angular
        .module('myapp')
        .controller('DisplayFormController', function($http,toastr,localStorageService) {
            var vm = this;
            vm.data=[];
            vm.forms = {};
            vm.elements = {};
            vm.user_id = localStorageService.get('user_id');
            vm.form_id = localStorageService.get('form_id');
            $http.get('/form_builder/forms/list_form.json?id='+vm.form_id) 

             .success(function(data){ 
               vm.forms = data.forms; 
            }); 

             
          $http.get('/form_builder/form_elements/list_element.json?id='+vm.form_id) 
           .success(function(data){ 
               vm.elements = data.elements; 
           }); 

           vm.submit = function(){ 

            angular.forEach(vm.elements,function(item){

               item.response['form_id']=vm.form_id;
               if(vm.user_id){
               item.response['user_id']=vm.user_id;}
              vm.data.push(item.response);
              
            });
            
            url = "/form_builder/form_responses/add_response.json"; 
            $http({ 
                    method : "POST", 
                    url : url, 
                    data :vm.data
                    }). 
                  then(function (data){ 
                    if(data.data.message == "save") {
                      toastr.success('Success', 'Data has been saved Successfully!');
                    } else {
                      toastr.error('Error', 'Something went wrong!');
                    }
                  });        
           } 


});

</script>
<div ng-app="myapp" ng-controller="DisplayFormController as vm"> 

<div class="row"> 
<div class="col-lg-8"> 

<h2>{{ vm.forms.name }}</h2>
<div ng-if="vm.forms.recstatus">
    <form name="myForm" class="well form-horizontal">
        <div ng-repeat="field in vm.elements" >
            <field-directive field="field">

            </field-directive>
        </div>
    </form>

    <div class="form-actions">
        <p class="text-center">
            <button class="btn btn-success right" type="button" ng-click="vm.submit()"><i class="icon-edit icon-white"></i> Submit Form</button>
           
        </p>
    </div>
</div>

</div> 
</div> 
</div> 
