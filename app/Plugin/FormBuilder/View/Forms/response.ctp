<script type="text/javascript">
 angular
        .module('myapp')
        .controller('DisplayFormController', function($http,toastr,localStorageService) {
            var vm = this;
            vm.data=[];
            vm.forms = {};
            vm.elements = {};
             vm.responses = {};
             vm.count=1;

            vm.user_id = localStorageService.get('user_id');

            vm.form_id = localStorageService.get('form_id');
            $http.get('/form_builder/forms/list_form.json?id='+vm.form_id) 
             .success(function(data){ 
               vm.forms = data.forms; 
            }); 

             
          $http.get('/form_builder/form_elements/list_element.json?id='+vm.form_id) 
           .success(function(data){ 
               vm.elements = data.elements; 
           }); 

            $http.get('/form_builder/form_responses/list_response.json?id='+vm.form_id) 
           .success(function(data){ 
               vm.responses = data.responses; 
           }); 





});

</script>
<div ng-app="myapp" ng-controller="DisplayFormController as vm"> 

<div class="row"> 
<div class="col-lg-8"> 

<h2>{{ vm.forms.name }}</h2>

<div ng-if="vm.forms.recstatus">
    <form name="myForm" class="well form-horizontal">
    <table cellspacing="4" cellpadding="4" class="table table-striped">
    <tr>
    <th>User ID</th>
   
      
        <th ng-repeat="field in vm.elements" >
            {{field.field_title}}
        
        </th>
      
        </tr>

        <tr ng-repeat="data in vm.responses">
        <td >{{data.user_id}}</td>
        <td>
            {{data.value}}
        
      
        </td>
      
        </tr>
        </table>
    </form>

{{vm.responses}}
</div> 

</div> 

</div> 
