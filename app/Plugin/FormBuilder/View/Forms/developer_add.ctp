<script type="text/javascript">
 angular
        .module('myapp')
        .controller('AddFormController', function($scope,$http,toastr) {
            var vm = this;
            vm.form_options=[];
            vm.form_elements = [];
            vm.form = {
              "FormElement": vm.form_elements
            };
                       
            vm.form.name = 'My Form';
            vm.form.department_id = null;
            vm.accordion = {}
            vm.accordion.oneAtATime = true;
            vm.institution={};
            vm.addField = {};
            vm.addField.new = "textfield";
            vm.addField.lastAddedID = 0;
            vm.departments = {};
            vm.categories = {}; 
            vm.close_date = null;
            vm.fields = {};
            
            $http.get('/form_builder/fields/list_fields.json') 
              .success(function(data){ 
                   vm.fields = data.fields; 
            }); 

            $http.get('/institutions/list_institutions.json') 
                .success(function(data){ 
                   vm.institutions = data.institutions; 
            }); 
     

            vm.getDepartment = function(){ 
              $http.get('/departments/list_add_departments.json?id='+vm.form.institution_id) 
                 .success(function(data){ 
                   vm.departments = data.departments; 
              }); 
            }
    

            vm.getCategory = function(){ 
              $http.get('/form_builder/categories/list_add_categories.json?id='+vm.form.department_id) 
                .success(function(data){ 
                  vm.categories = data.categories; 
              }); 
            } 

            vm.addNewField = function(){
              vm.addField.lastAddedID++;

              var newField = {
                  "field_id" : vm.addField.lastAddedID,
                  "field_title" : "New field - " + (vm.addField.lastAddedID),
                  "field_type" : vm.addField.new,
                  "default_value" : "",
                  "field_required" : false,
                  "field_disabled" : false
              };
              
              vm.form_elements.push(newField);
            }

            vm.showAddOptions = function (field){
                if(field.field_type == "radio" || field.field_type == "checkbox")
                    return true;
                else
                    return false;
            }

            vm.addOption = function (field){

                if(!field.FormOption)   

                    field.FormOption = new Array();

                var lastOptionID = 0;

                if(field.FormOption[field.FormOption.length-1])
                    lastOptionID = field.FormOption[field.FormOption.length-1].option_id;

                var option_id = lastOptionID + 1;

                var newOption = {
                    "option_id" : option_id,
                    "option_title" : "Option " + option_id,
                    "option_value" : option_id
                };

                field.FormOption.push(newOption);

            }

            vm.deleteField = function (field_id){
            for(var i = 0; i < vm.form_elements.length; i++){
                if(vm.form_elements[i].field_id == field_id){
                    vm.form_elements.splice(i, 1);
                    break;
            }
        }
    }


   vm.deleteOption = function (field, option){
      for(var i = 0; i < field.FormOption.length; i++){
        if(field.FormOption[i].option_id == option.option_id){
          field.FormOption.splice(i, 1);// 1st parameter i is starting index, 2nd parameter denotes upto howmany elements you want to remove
          break;
        }
      }
  }
            
          vm.submitForm = function(){ 
    
    url = "/form_builder/forms/form_add"; 
    $http({ 
            method : "POST", 
            url : url, 
            data :vm.form
            }). 
          then(function (data){ 
            if(data.data.message == "save") {
              toastr.success('Success', 'Data has been saved Successfully!');
            } else {
              toastr.error(data.data.message);
            }
          });        
   } 
          

          vm.open = function($event) {
            vm.status.opened = true;
          };

          vm.format = 'mediumDate';

          vm.status = {
            opened: false
          };
});

</script>
<div ng-app="myapp" ng-controller="AddFormController as vm"> 
<div class="row"> 
<div class="col-lg-8"> 

  <form name="createForm" class="well form-horizontal" > 
    <fieldset> 
      <legend>Create Form</legend> 
        <div class="form-group"> 

          <h4><label for="FormProperties">Form Properties</label></h4> <br>

            <label for="FormName" >Form Name</label> 

                <input type="text" class="form-control" ng-model="vm.form.name"> <br>
         <div class="form-group"> 
         <div class="col-lg-12"> 

          <label for="institution">Instituton</label> 
              <select id="institution" class="form-control" ng-options="key as value for (key,value) in vm.institutions" ng-change="vm.getDepartment()" ng-model="vm.form.institution_id"> 
                <option value = "">Please Select First</option> 
              </select> 
              </div>
        </div> 
          <div class="form-group"> 
          <div class="col-lg-12"> 
      
           <label for="department">Department</label> 
              <select id="department" class="form-control" ng-options="key as value for (key,value) in vm.departments" ng-change="vm.getCategory()" ng-model="vm.form.department_id" > 
                <option value = "">Please Select</option> 
              </select>           
              </div>
              </div> 
          <div class="form-group"> 
          <div class="col-lg-12"> 
          <label for="category">Category</label> 
              <select id="category" class="form-control" ng-options="key as value for (key,value) in vm.categories" ng-model="vm.form.category_id">    
              <option value = "">Please Select</option>         
              </select> 
              </div>
        </div> 
        <div class="form-group">
        <div class="col-lg-12"> 
          <label for="closeDate">Close Date</label> 
          <p class="input-group">
            <div class="dropdown"> 
            <a class="dropdown-toggle my-toggle-select" id="dLabel" role="button" data-toggle="dropdown" data-target="#" href=""> 
             
              <div class="col-md-10">  <input type="text" class="form-control" datepicker-popup="{{vm.format}}" ng-model="vm.form.close_date" is-open="vm.status.opened" datepicker-options="dateOptions"/>
              </div>
              <div class="col-md-2">
                <span class="input-group-btn">
                  <button type="button" class="btn btn-default" ng-click="vm.open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
            </a>
                </span>
                </div>
              </div>
          </p>
            </div>
        </div>
          <h4><label for="FormFields">Form Fields</label></h4> <br>
             
          <div class="add-field">
             <select class="form-control" ng-model="vm.addField.new" ng-options="value as value for (key,value) in vm.fields"></select>
                 <br>
             <button type="submit" class="btn btn-primary" ng-click="vm.addNewField()">Add Field</button>
          </div>
            <br>
        <p ng-show="vm.form_elements.length == 0">No fields added yet.</p>
        <accordion close-others="accordion.oneAtATime">
            <accordion-group heading="{{field.field_title}}" ng-repeat="field in vm.form_elements">
              <div class="clear"></div> <hr>
                <div class="accordion-edit">
                     <button class="btn btn-danger pull-right" type="button" ng-click="vm.deleteField(field.field_id)"><i class="icon-trash icon-white"></i> Delete</button>
                    <div class="row">
                        <div class="span2" id="field"><h4>Field ID: </h4></div>
                        <div class="span4" id="field">{{field.field_id}}</div>
                    </div>
                    <div class="row">
                        <div class="span2" id="field"><h4>Field Type:</h4></div>
                        <div class="span4" id="field">{{field.field_type}}</div>
                    </div>

                    <div class="clear"></div> <hr>

                    <div class="row">
                        <div class="span2" id="field"><h4>Field Title:</h4></div>
                        <div class="span4" id="field"><input type="text" ng-model="field.field_title" value="{{field.field_title}}" class="form-control"></div>
                    </div>
                    <div class="row">
                        <div class="span2" id="field"><h4>Field Default Value:</h4></div>
                        <div class="span4" id="field"><input type="text" ng-model="field.default_value" value="{{field.default_value}}" class="form-control"></div>
                    </div>
                    <div class="row" ng-show="vm.showAddOptions(field)">
                        <div class="span2" id="field"><h4>Field Options:</h4></div>
                        <div class="span6" id="field">
                            <div ng-repeat="option in field.FormOption">
                                <input type="text" ng-model="option.option_title" value="{{option.option_title}}">
                                <a class="btn btn-danger btn-mini right" type="button" ng-click="vm.deleteOption(field, option)"><i class="glyphicon glyphicon-remove"></i></a>
                               
                            </div>
                            <button class="btn btn-primary btn-small" type="button" ng-click="vm.addOption(field)"><i class="icon-plus icon-white"></i> Add Option</button>
                        </div>
                    </div> 
                    <div class="clear"></div> <hr>

                    <div class="row">
                        <div class="span2" id="field"><h4>Required:</h4></div>
                        <div class="span4" id="field">
                            <label>
                                <input type="radio" ng-value="true" ng-model="field.field_required" id="field"/>
                                &nbsp; Yes
                            </label>
                            <label>
                                <input type="radio" ng-value="false" ng-model="field.field_required" id="field"/>

                                &nbsp; No
                            </label>
                        </div>
                    </div>
                    
                    <div class="clear"></div> <hr>

                    <div class="row">
                        <div class="span2" id="field"><h4>Disabled:</h4></div>
                        <div class="span4" id="field">
                            <label>
                                <input type="radio" ng-value="true" ng-selected ng-model="field.field_disabled" id="field"/>
                                &nbsp; Yes
                            </label>
                            <label>
                                <input type="radio" ng-value="false" ng-model="field.field_disabled" id="field" id="field"/>
                                &nbsp; No
                            </label>
                        </div>
                    </div>
                  </div> 
                </div>
            </accordion-group>
        </accordion>
      <button ng-click="vm.submitForm()" class="btn btn-primary">Submit</button> 
<span>
</span>

 </fieldset> 
</form> 

</div> 

</div> 
</div> 
