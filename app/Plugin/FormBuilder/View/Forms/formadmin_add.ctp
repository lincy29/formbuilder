
<script type="text/javascript">
 angular
        .module('myapp')
        .controller('MyController', function($scope,$http,localStorageService,toastr) {
            
            $scope.form = {};
            $scope.form.name = 'My Form';
            $scope.form.department_id = null;
            $scope.form.form_fields = [];
            $scope.accordion = {}
            $scope.accordion.oneAtATime = true;
            $scope.institution={};
            $scope.addField = {};
            $scope.addField.new = "TextBox";
            $scope.addField.lastAddedID = 0;
            $scope.departments = {};
            $scope.categories = {}; 
            $scope.close_date = null;
            
            $scope.fields = {};

          $http.get('/form_builder/fields/list_fields.json') 
           .success(function(data){ 
               $scope.fields = data.fields; 
          }); 

          $scope.form.institution_id=localStorageService.get('institution_id');
           $http.get('/departments/list_add_departments.json?id='+$scope.form.institution_id) 
                 .success(function(data){ 
                   $scope.departments = data.departments; 
            }); 
         
    

   $scope.getCategory = function(){ 
          $http.get('/form_builder/categories/list_add_categories.json?id='+$scope.form.department_id) 
          .success(function(data){ 
             $scope.categories = data.categories; 
     
              }); 
        } 

    $scope.addNewField = function(){

        $scope.addField.lastAddedID++;

        var newField = {
            "field_id" : $scope.addField.lastAddedID,
            "field_title" : "New field - " + ($scope.addField.lastAddedID),
            "field_type" : $scope.addField.new,
            "default_value" : "",
            "required" : false,
            "disabled" : false

        };

        $scope.form.form_fields.push(newField);
    }

    $scope.showAddOptions = function (field){
        if(field.field_type == "Radio" || field.field_type == "CheckBox")
            return true;
        else
            return false;
    }

    $scope.addOption = function (field){
        if(!field.field_options)   

            field.field_options = new Array();

        var lastOptionID = 0;

        if(field.field_options[field.field_options.length-1])
            lastOptionID = field.field_options[field.field_options.length-1].option_id;

        var option_id = lastOptionID + 1;

        var newOption = {
            "option_id" : option_id,
            "option_title" : "Option " + option_id,
            "option_value" : option_id
        };

        field.field_options.push(newOption);
    }

    $scope.deleteOption = function (field, option){
        var index = field.field_options.indexOf(option);
        field.field_options.splice(index, 1);
   }

   $scope.submitForm = function(){ 
    
    url = "/form_builder/forms/form_add"; 
    $http({ 
            method : "POST", 
            url : url, 
            data :$scope.form
            }). 
          then(function (data){ 
            if(data.data.message == "save") {
              toastr.success('Success', 'Data has been saved Successfully!');
            } else {
              toastr.error('Error', 'Something went wrong!');
            }
          });        
   } 

  

   $scope.open = function($event) {
          $scope.status.opened = true;
        };

          $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
          };

          $scope.format = 'shortDate';

  
          $scope.status = {
           opened: false
          };
});

</script>
<div ng-app="myapp" ng-controller="MyController"> 
<div class="row"> 
<div class="col-lg-8"> 

  <form name="createForm" class="well form-horizontal" > 
    <fieldset> 
      <legend>Create Form</legend> 
        <div class="form-group"> 

          <h4><label for="FormProperties">Form Properties</label></h4> <br>

            <label for="FormName" >Form Name</label> 
                <input type = "text" class="form-control" ng-model="form.name"> <br>

          <div class="form-group"> 
          <div class="col-lg-12"> 
      
           <label for="department">Department</label> 
              <select id="department" class="form-control" ng-options="key as value for (key,value) in departments" ng-change="getCategory()" ng-model="form.department_id" required> 
                <option value = "">Please Select First</option> 
              </select>           
              </div>
              </div> 
          <div class="form-group"> 
          <div class="col-lg-12"> 
          <label for="category">Category</label> 
              <select id="category" class="form-control" ng-options="key as value for (key,value) in categories" ng-model="form.category_id" required>    
              <option value = "">Select Department First</option>         
              </select> 
              </div>
        </div> 
        <div class="form-group">
        <div class="col-lg-12"> 
          <label for="closeDate">Close Date</label> 
          <p class="input-group">
            <div class="dropdown"> 
            <a class="dropdown-toggle my-toggle-select" id="dLabel" role="button" data-toggle="dropdown" data-target="#" href=""> 
             
              <div class="col-md-10">  <input type="text" class="form-control" datepicker-popup="{{format}}" ng-model="form.close_date" is-open="status.opened" datepicker-options="dateOptions"/>
              </div>
              <div class="col-md-2">
                <span class="input-group-btn">
                  <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
            </a>
                </span>
                </div>
              </div>
          </p>
            </div>
        </div>
          <h4><label for="FormFields">Form Fields</label></h4> <br>
            
          <div class="add-field">
              <select class="form-control" ng-model="addField.new" ng-options="value as value for (key,value) in fields"></select>
                 <br>
             <button type="submit" class="btn btn-primary" ng-click="addNewField()">Add Field</button>
          </div>
            <br>
        <p ng-show="form.form_fields.length == 0">No fields added yet.</p>
        <accordion close-others="accordion.oneAtATime">
            <accordion-group heading="{{field.field_title}}" ng-repeat="field in form.form_fields">
              <div class="clear"></div> <hr>
                <div class="accordion-edit">
                    
                    <div class="row">
                        <div class="span2" id="field"><h4>Field ID: </h4></div>
                        <div class="span4" id="field">{{field.field_id}}</div>
                    </div>
                    <div class="row">
                        <div class="span2" id="field"><h4>Field Type:</h4></div>
                        <div class="span4" id="field">{{field.field_type}}</div>
                    </div>

                    <div class="clear"></div> <hr>

                    <div class="row">
                        <div class="span2" id="field"><h4>Field Title:</h4></div>
                        <div class="span4" id="field"><input type="text" ng-model="field.field_title" value="{{field.field_title}}" class="form-control"></div>
                    </div>
                    <div class="row">
                        <div class="span2" id="field"><h4>Field Default Value:</h4></div>
                        <div class="span4" id="field"><input type="text" ng-model="field.field_value" value="{{field.field_value}}" class="form-control"></div>
                    </div>
                    <div class="row" ng-show="showAddOptions(field)">
                        <div class="span2" id="field"><h4>Field Options:</h4></div>
                        <div class="span6" id="field">
                            <div ng-repeat="option in field.field_options">
                                <input type="text" ng-model="option.option_title" value="{{option.option_title}}">
                                <a class="btn btn-danger btn-mini right" type="button" ng-click="deleteOption(field, option)"><i class="glyphicon glyphicon-remove"></i></a>
                               
                            </div>
                            <button class="btn btn-primary btn-small" type="button" ng-click="addOption(field)"><i class="icon-plus icon-white"></i> Add Option</button>
                        </div>
                    </div> 
                    <div class="clear"></div> <hr>

                    <div class="row">
                        <div class="span2" id="field"><h4>Required:</h4></div>
                        <div class="span4" id="field">
                            <label>
                                <input type="radio" ng-value="true" ng-model="field.field_required" id="field"/>
                                &nbsp; Yes
                            </label>
                            <label>
                                <input type="radio" ng-value="false" ng-model="field.field_required" id="field"/>

                                &nbsp; No
                            </label>
                        </div>
                    </div>
                    
                    <div class="clear"></div> <hr>

                    <div class="row">
                        <div class="span2" id="field"><h4>Disabled:</h4></div>
                        <div class="span4" id="field">
                            <label>
                                <input type="radio" ng-value="true" ng-selected ng-model="field.field_disabled" id="field"/>
                                &nbsp; Yes
                            </label>
                            <label>
                                <input type="radio" ng-value="false" ng-model="field.field_disabled" id="field" id="field"/>
                                &nbsp; No
                            </label>
                        </div>
                    </div>
                  </div> 
                </div>
            </accordion-group>
        </accordion>
      <button ng-click="submitForm()" class="btn btn-primary">Submit</button> 
<span>
</span>

 </fieldset> 
</form> 

</div> 

</div> 
</div> 

