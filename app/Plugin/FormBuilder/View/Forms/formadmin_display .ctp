<script type="text/javascript">
 angular
        .module('myapp')
        .controller('DisplayFormController', function($scope,$http,toastr,localStorageService,$window) {
            var vm = this;
            vm.departments = {};
            vm.categories = {}; 
            vm.forms={};
            vm.count=0;
            

            vm.form.institution_id=localStorageService.get('institution_id');
     

            vm.getDepartment = function(){ 
              $http.get('/departments/list_add_departments.json?id='+vm.form.institution_id) 
                 .success(function(data){ 
                   vm.departments = data.departments; 
              }); 
            }
    

            vm.getCategory = function(){ 
              $http.get('/form_builder/categories/list_add_categories.json?id='+vm.form.department_id) 
                .success(function(data){ 
                  vm.categories = data.categories; 
              }); 
            } 

             vm.getForm = function(){ 
              $http.get('/form_builder/forms/list_forms.json?id='+vm.form.category_id)
                .success(function(data){ 
                  vm.forms = data.forms; 
              }); 
            } 

            vm.submitForm = function(){ 
            localStorageService.set('form_id',vm.forms.form_id);
            url = "/form_builder/forms/view";
             $window.location.href = url;   
        
           } 
      
});

</script>
<div ng-app="myapp" ng-controller="DisplayFormController as vm"> 
<div class="row"> 
<div class="col-lg-8"> 

  <form name="DisplayForm" class="well form-horizontal" > 
    <fieldset> 
      <legend>Display Form</legend> 
        <div class="form-group"> 

         
          <div class="form-group"> 
          <div class="col-lg-12"> 
      
           <label for="department">Department</label> 
              <select id="department" class="form-control" ng-options="key as value for (key,value) in vm.departments" ng-change="vm.getCategory()" ng-model="vm.form.department_id" > 
                <option value = "">Please Select</option> 
              </select>           
              </div>
              </div> 
          <div class="form-group"> 
          <div class="col-lg-12"> 
          <label for="category">Category</label> 
              <select id="category" class="form-control" ng-options="key as value for (key,value) in vm.categories" ng-model="vm.form.category_id" ng-change="vm.getForm()">    
              <option value = "">Please Select</option>         
              </select> 
              </div>
        </div> 

        <div class="form-group"> 
          <div class="col-lg-12"> 
          <label for="category">Form</label> 
              <select id="category" class="form-control" ng-options="key as value for (key,value) in vm.forms" ng-model="vm.forms.form_id">    
              <option value = "">Please Select</option>         
              </select> 
              </div>
        </div> 

          
 <button class="btn btn-primary" ng-click="vm.submitForm()">Submit</button> 
 
 </fieldset> 

</form> 

</div> 

</div> 
</div> 
