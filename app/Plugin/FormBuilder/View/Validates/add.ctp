<script type="text/javascript">
 angular
       .module('myapp')
        .controller('ValidateController', function($http,toastr) {
            var vm = this;

            vm.validate = {};
            vm.submitForm = function(){ 
            
            url = "/form_builder/validates/add.json"; 
            $http({ 
                    method : "POST", 
                    url : url, 
                    data :vm.validate
                    }). 
                  then(function (data){ 
                    if(data.data.message == "save") {
                      toastr.success('Success', 'Data has been saved Successfully!');
                    } else {
                      toastr.error(data.data.message.name[0]);
                    }
                  });        
           } 
});

</script>
<div ng-app="myapp" ng-controller="ValidateController as vm"> 
<div class="row"> 
<div class="col-lg-8"> 

  <form name="createForm" class="well form-horizontal" > 
    <label for="FormName" >Form Name</label> 
      <input type="text" class="form-control" ng-model="vm.validate.name"> <br>      
  <button ng-click="vm.submitForm()" class="btn btn-primary">Submit</button>
  </form>
</div>
</div>
</div>