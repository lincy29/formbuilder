<!DOCTYPE html>
<html lang="en" ng-app="myapp">
  <head>
	<title>
		<?php echo $title_for_layout; ?>
	</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
	<?php
		echo $this->Html->meta('icon');
	?>

  <?php
    echo $this->Html->css('FormBuilder.bootstrap.min');
    echo $this->Html->css('FormBuilder.angular-toastr');
    echo $this->fetch('meta');
    echo $this->fetch('css');
    echo $this->Html->script('FormBuilder.jquery.min');
    echo $this->Html->script('FormBuilder.angular');
    echo $this->Html->script('fade');
    echo $this->Html->script('FormBuilder.bootstrap.min');
    echo $this->Html->script('FormBuilder.ui-bootstrap-tpls-0.13.4');
    echo $this->Html->script('FormBuilder.angular-local-storage.min');
    echo $this->Html->script('FormBuilder.angular-toastr.tpls');
    echo $this->Html->script('FormBuilder.angular-route');
    echo $this->Html->script('FormBuilder.app'); 
    echo $this->Html->script('FormBuilder.field-directive');     
    echo $this->fetch('script');
  ?>
  </head>
  <body>
    <div class="row" style="width:100%;">
      <div class="span4">
        <?php 
            if($this->Session->check('Auth.User.id')) {
                echo $this->Element('navigation');
            } else {
                echo $this->Element('login');
            }
        ?>
       </div>
    </div>
    
    <div class="container">
			<?php echo $this->Session->flash(); ?>
			<?php echo $this->fetch('content'); ?>
    </div>
  </body>
</html>
