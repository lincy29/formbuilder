<?php echo $this->Html->css('FormBuilder.navigation'); ?>
<br>
<div class="navbar navbar-default navbar-static-top" style="background-color:#fff;">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        <p style="padding-left: 20px;"><?php echo $this->Html->image('gnulogo.png', array('alt' => 'GNU', 'border' => '0')); ?></p>
        </div>
        <h2 style="padding-left:200px;">Dynamic Form Generator</h2>
    <div class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <li>
          <?php echo $this->Html->link(__("Home"),array('developer'=>false,
                                                        'plugin'=>false,
                                                        'controller' => 'users',
                                                        'action' => 'dashboard'));
           ?>
        </li>


<?php if(Auth::hasRoles(['developer','superadmin','admin'])) {?>
  <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Categories <span class="caret"></span></a>
    <ul class="dropdown-menu">
      <li><?php echo $this->Html->link(__("New Category",true),[
                      'developer'=>true,
                      'plugin'=>'form_builder',
                      'controller' => 'categories', 
                      'action' => 'add']); ?>
      </li>
      <li><?php echo $this->Html->link(__("View Category",true),[
                      'developer'=>true,
                      'plugin'=>'form_builder',
                      'controller' => 'categories',
                      'action' => 'index']);  ?>
      </li>
    </ul>
  </li>
<?php } ?>
<?php if(Auth::hasRoles(['formadmin'])) {?>
  <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Categories <span class="caret"></span></a>
    <ul class="dropdown-menu">
      <li><?php echo $this->Html->link(__("New Category",true),[
                      'formadmin'=>true,
                      'plugin'=>'form_builder',
                      'controller' => 'categories', 
                      'action' => 'add']); ?>

      </li>
      <li><?php echo $this->Html->link(__("View Category",true),[
                      'formadmin'=>true,
                      'plugin'=>'form_builder',
                      'controller' => 'categories',
                      'action' => 'index']);  ?>

      </li>
    </ul>
  </li>
<?php } ?>

<?php if(Auth::hasRoles(['formcoordinator'])) {?>
  <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Categories <span class="caret"></span></a>
    <ul class="dropdown-menu">
      <li><?php echo $this->Html->link(__("New Category",true),[
                      'formcoordinator'=>true,
                      'plugin'=>'form_builder',
                      'controller' => 'categories', 
                      'action' => 'add']); ?>
      </li>
      <li><?php echo $this->Html->link(__("View Category",true),[
                      'formcoordinator'=>true,
                      'plugin'=>'form_builder',
                      'controller' => 'categories',
                      'action' => 'index']);  ?>
      </li>
    </ul>
  </li>
<?php } ?>

<?php if(Auth::hasRoles(['developer','superadmin','admin'])) {?>
  <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Forms <span class="caret"></span></a>
    <ul class="dropdown-menu">
      <li><?php echo $this->Html->link(__("New Form",true),[
                      'developer'=>true,
                      'plugin'=>'form_builder',
                      'controller' => 'forms', 
                      'action' => 'add']); ?>
      </li>
      <li><?php echo $this->Html->link(__("View Form",true),[
                      'developer'=>true,
                      'plugin'=>'form_builder',
                      'controller' => 'forms',
                      'action' => 'index']);  ?>
      </li>
      <li><?php echo $this->Html->link(__("Submit Response",true),[
                      'developer'=>true,
                      'plugin'=>'form_builder',
                      'controller' => 'forms',
                      'action' => 'display']);  ?>
      </li>
      <li><?php echo $this->Html->link(__("view Response",true),[
                      
                      'plugin'=>'form_builder',
                      'controller' => 'forms',
                      'action' => 'display_response']);  ?>
      </li>
    </ul>
  </li>
<?php } ?>


<?php if(Auth::hasRoles(['formadmin'])) {?>
  <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Forms <span class="caret"></span></a>
    <ul class="dropdown-menu">
      <li><?php echo $this->Html->link(__("New Form",true),[
                      'formadmin'=>true,
                      'plugin'=>'form_builder',
                      'controller' => 'forms', 
                      'action' => 'add']); ?>

      </li>
      <li><?php echo $this->Html->link(__("View Form",true),[
                      'formadmin'=>true,
                      'plugin'=>'form_builder',
                      'controller' => 'forms',
                      'action' => 'index']);  ?>

      </li>
      <li><?php echo $this->Html->link(__("Submit Response",true),[
                      'formadmin'=>true,
                      'plugin'=>'form_builder',
                      'controller' => 'forms',
                      'action' => 'display']);  ?>
      </li>
    </ul>
  </li>
<?php } ?>

<?php if(Auth::hasRoles(['formcoordinator'])) {?>
  <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Forms <span class="caret"></span></a>
    <ul class="dropdown-menu">
      <li><?php echo $this->Html->link(__("New Form",true),[
                      'formcoordinator'=>true,
                      'plugin'=>'form_builder',
                      'controller' => 'forms', 
                      'action' => 'add']); ?>

      </li>
      <li><?php echo $this->Html->link(__("View Form",true),[
                      'formcoordinator'=>true,
                      'plugin'=>'form_builder',
                      'controller' => 'forms',
                      'action' => 'index']);  ?>

      </li>
      <li><?php echo $this->Html->link(__("Submit Response",true),[
                      'formcoordinator'=>true,
                      'plugin'=>'form_builder',
                      'controller' => 'forms',
                      'action' => 'display']);  ?>
      </li>
    </ul>
  </li>
<?php } ?>


<?php if(Auth::hasRoles(['staff','student'])) {?>
  <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Forms <span class="caret"></span></a>
    <ul class="dropdown-menu">

      <li><?php echo $this->Html->link(__("Submit Response",true),[
                      'student'=>true,
                      'plugin'=>'form_builder',
                      'controller' => 'forms',
                      'action' => 'display']);  ?>
      </li>
    </ul>
  </li>
<?php } ?>

          <li>
              <?php echo $this->Html->link(__("Logout",true),[
              'developer'=>false,
              'controller' => 'users' ,
              'action'=>'logout' ,
              'plugin'=>false]); ?>
          </li> 
      </div>
    </div>
</div>