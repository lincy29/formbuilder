<script type="text/javascript"> 
  angular
        .module('myapp')
        .controller('categoryController',function($scope,$http,toastr){ 
         $scope.institutions = {}; 
	     $scope.categories= {}; 
       $scope.categories.category_name="";
       
	            
         $http.get('/institutions/list_institutions.json') 
           .success(function(data){ 
               $scope.institutions = data.institutions; 
          }); 
     

         $scope.getDepartment = function(){ 
           $http.get('/departments/list_add_departments.json?id='+$scope.categories.institution_id) 
                 .success(function(data){ 
                   $scope.departments = data.departments; 
            }); 
         }

         $scope.submit = function(){ 
          url = "/form_builder/categories/add"; 
          $http({ 
            method : "POST", 
            url : url, 
            data : $scope.categories 
            }). 
          then(function (data){ 
            if(data.data.message == "save") {
              toastr.success('Success', 'Data has been saved Successfully!');
            } else {
              toastr.error(data.data.message);
            }
          }); 
        }

 }); 
      
</script> 

<div ng-app="myapp" ng-controller="categoryController"> 
<div class="row"> 
<div class="col-lg-8"> 
<div class="tickets form"> 
  <form name="billform" class="well form-horizontal" > 
    <fieldset> 
      <legend>Category</legend> 
        <div class="form-group"> 
          <label for="institution">Instituton</label> 
              <select id="institution" class="form-control" ng-options="key as value for (key,value) in institutions" ng-change="getDepartment()" ng-model="categories.institution_id"> 
                <option value = "">Please Select First</option> 
              </select> 
        </div> 
        <div class="form-group"> 
          <label for="department">Department</label> 
              <select id="department" class="form-control" ng-options="key as value for (key,value) in departments" ng-change="getCategory()" ng-model="categories.department_id"> 
              <option value = "">Select Institution First</option> 
              </select> 
        </div> 
        <div class="form-group"> 
          <label for="category">Category</label> 
             <input type="text"  class="form-control" ng-model="categories.category_name"> 
        </div>
        <button type="button" ng-click="submit()" class="btn btn-primary">Submit 
    </button> 
 </fieldset> 
  </form> 
</div> 
</div> 
</div> 
</div> 
     